import re

test0='CMZ'
test1='MCD'

def parse_input( input_file,step):
    with open( input_file, "r") as fd:
        lines = [line.replace('\n','') for line in fd]
    #get the number of the line which state the column
    not_bracket=True
    for i, li in enumerate(lines):
        #print(li)
        if li[0]=='[':
            not_bracket=False
        if not not_bracket and li[0]==' ':
            line_number = i
            break
    # position of stack in the input
    stack_pos = {key:value for (key,value) in [(x,lines[line_number].find(x)) for x in re.findall('[0-9]+', lines[line_number]) ]}    
    crade_in_stack = {key:value for (key,value) in [ (x, [li[stack_pos[x]] for li in lines[0:line_number]if li[stack_pos[x]]!=' ']) for x in stack_pos.keys()]}
    # instructions
    ins = [re.findall('[0-9]+',li) for li in lines[line_number+2:]]
    return crade_in_stack, ins

def move_cradle(crade_in_stack, ins, model='9000'):
    for nb, sout, sin in ins:
        if model=='9000':
            for i in range(int(nb)):
                crade_in_stack[sin].insert(0, crade_in_stack[sout][0])
                del crade_in_stack[sout][0]
        elif model=='9001':
            for elt in crade_in_stack[sout][0:int(nb)][::-1]:
                crade_in_stack[sin].insert(0, elt)
            for i in range(int(nb)):
                del crade_in_stack[sout][0]
    return crade_in_stack

def calc1( input_file):
    crade_in_stack, ins = parse_input( input_file,'1')
    crade_in_stack = move_cradle(crade_in_stack, ins)
    return ''.join([crad[0] for crad in crade_in_stack.values()])

def calc2( input_file):
    crade_in_stack, ins = parse_input( input_file,'1')
    crade_in_stack = move_cradle(crade_in_stack, ins, model='9001')
    return ''.join([crad[0] for crad in crade_in_stack.values()])

print("Test validated: ", test0 == calc1( 'test'))
print(calc1( 'input'))

print("Test validated: ", test1 == calc2( 'test'))
print(calc2( 'input'))
