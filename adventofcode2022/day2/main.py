
test0=15
test1=12

dic_fpc={'A': 1, 'B':2, 'C':3, 'X':1, 'Y':2, 'Z':3}
dic_fpc_2={'A': 1, 'B':2, 'C':3, 'X':-1, 'Y':0, 'Z':+1}
dic_fpc_win = {'X':0, 'Y':3, 'Z':6}

def parse_input( input_file,step):
    with open( input_file, "r") as fd:
        #fily = fd.read() 
        lines = [line.strip()  for line in fd]
    #list_cal = [x.split('\n') for x in fily.split('\n\n')]
    stats = [ line.split(' ') for line in lines]
    stat_1 = [ dic_fpc[x[0]] for x in stats]
    if step=='1':
        stat_2 = [ dic_fpc[x[1]] for x in stats]
    else:
        stat_2 = [ dic_fpc_2[x[1]] for x in stats]
        stat_2 = [ stat_1[i] + stat_2[i] for i in range(len(stat_2))]
        stat_2 = [ [stat_2[i], 1][stat_2[i]==4] for i in range(len(stat_2))]
        stat_2 = [ [stat_2[i], 3][stat_2[i]==0] for i in range(len(stat_2))]
    return [stat_1, stat_2]

def score(cal):
    score_choice=sum(cal[1])
    s1, s2 = cal[0], cal[1] 
    score_win = [0 for x in s1]
    score_win = [score_win[i] + [0,6][s1[i]==3 and s2[i]==1]  for i in range(len(s1))]
    score_win = [score_win[i] + [0,6][s1[i]==1 and s2[i]==2]  for i in range(len(s1))]
    score_win = [score_win[i] + [0,6][s1[i]==2 and s2[i]==3]  for i in range(len(s1))]
    score_win = [score_win[i] + [0,3][s1[i]==s2[i]]  for i in range(len(s1))]
    #print(score_win)
    #print(s2)
    return score_choice+sum(score_win)

def calc1( input_file):
    cal = parse_input( input_file,'1')
    return score( cal)

def calc2( input_file):
    cal = parse_input( input_file,'2')
    return score( cal)

print("Test validated: ", test0 == calc1( 'test'))
print(calc1( 'input'))

print("Test validated: ", test1 == calc2( 'test'))
print(calc2( 'input'))
