
test0=24000
test1=45000

def parse_input( input_file):
    with open( input_file, "r") as fd:
        fily = fd.read() #a[line.strip()  for line in fd]
    list_cal = [x.split('\n') for x in fily.split('\n\n')]
    list_cal = [ [int(x) for x in y if x != ''] for y in list_cal]
    return list_cal

def max_cal(cal):
    max_cal= 0
    for x in cal:
        max_cal = max( max_cal, sum(x))
    return(max_cal)

def max_cal_3(cal):
    all_sum = [sum(x) for x in cal]
    max_3 = list()
    for i in range(3):
        maxy = max(all_sum)
        max_3.append(maxy)
        all_sum.remove(maxy)
    return sum(max_3)


def calc1( input_file):
    cal = parse_input( input_file)
    return max_cal( cal)

def calc2( input_file):
    cal = parse_input( input_file)
    return max_cal_3( cal)

print("Test validated: ", test0 == calc1( 'test'))
print(calc1( 'input'))

print("Test validated: ", test1 == calc2( 'test'))
print(calc2( 'input'))
