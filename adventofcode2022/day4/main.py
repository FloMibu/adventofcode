
test0=2
test1=12


def parse_input( input_file,step):
    cal = 0
    with open( input_file, "r") as fd:
        #fily = fd.read() 
        lines = [line.strip()  for line in fd]
    for i, line in enumerate(lines):
        s1, s2 = line.split(',')[0].split('-'), line.split(',')[1].split('-')
        print(i, s1,s2, [0,1][ (int(s1[0]) >= int(s2[0]) and int(s1[1]) <= int(s2[1])) or (int(s1[0]) <= int(s2[0]) and int(s1[1]) >= int(s2[1]))])
        cal += [0,1][ ( (int(s1[0]) >= int(s2[0])) and (int(s1[1]) <= int(s2[1]))) or ((int(s1[0]) <= int(s2[0])) and (int(s1[1]) >= int(s2[1])))]
        #cal -= [0,1][ (int(s1[0]) == int(s2[0]) and int(s1[1]) == int(s2[1]))]
    return cal

def calc1( input_file):
    cal = parse_input( input_file,'1')
    return cal

def calc2( input_file):
    cal = parse_input( input_file,'2')
    return cal

print("Test validated: ", test0 == calc1( 'test'))
print(calc1( 'input'))

#print("Test validated: ", test1 == calc2( 'test'))
#print(calc2( 'input'))
