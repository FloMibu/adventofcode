from string import ascii_letters

test0=157
test1=70


def calc1_loops( input_file):
    with open( input_file, "r") as fd:
        #fily = fd.read() 
        lines = [line.strip()  for line in fd]
    numL=0    
    for li in lines:
        print(li,len(li), (len(li)/2+1))
        s1, s2 = li[0:(int(len(li)/2))], li[(int(len(li)/2)):]
        letter = [x for x in s1 if x in s2]
        print(li,letter)
        numL += ascii_letters.find(letter[0]) +1
    return numL

def calc1_oneline(input_file):
    with open( input_file, "r") as fd:
        lines = [line.strip()  for line in fd]
    return sum( [ascii_letters.find([x for x in li[0:(int(len(li)/2))] if x in li[(int(len(li)/2)):]][0]) +1 for li in lines])

def calc2(input_file):
    with open( input_file, "r") as fd:
        lines = [line.strip()  for line in fd]
    nb_lines = len(lines)
    out = 0
    for i in range( int(len(lines)/3)):
        com_12 = [x for x in lines[i*3] if x in lines[i*3+1]]
        com_123 = [x for x in com_12 if x in lines[i*3+2]]
        out += ascii_letters.find(com_123[0]) +1
    return out

print("Test validated: ", test0 == calc1_loops( 'test'))
print(calc1_loops( 'input'))
print(calc1_oneline( 'input'))

print("Test validated: ", test1 == calc2( 'test'))
print(calc2( 'input'))
