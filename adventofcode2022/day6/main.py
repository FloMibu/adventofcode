import re

test0={'mjqjpqmgbljsphdztnvjfqwrcgsmlb':[7,19],
        'bvwbjplbgvbhsrlpgdmjqwftvncz':[5,23],
        'nppdvjthqldpwncqszvftbrmjlhg':[6,23],
        'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg':[10,29],
        'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw':[11,26]}

def parse_input( input_file,step):
    with open( input_file, "r") as fd:
        lines = [line.replace('\n','') for line in fd]
    return lines[0]

def getmarker(stry,nb):
    for i in range(len(stry)):
        if len(set(stry[i:i+nb]))==nb:
            break
    return i+nb

def calc1( input_file,nb):
    stry = parse_input( input_file,'1')
    return getmarker(stry,nb)

for key, val in test0.items():
    print("Test validated: ", val[0] == getmarker(key,4) )
print(calc1( 'input',4))

for key, val in test0.items():
    print("Test validated: ", val[1] == getmarker(key,14) )
print(calc1( 'input',14))

