import numpy as np

#############################
# day 10 

test1=7*5
test12=22*10
test2=8
test22 = 19208

def crea_list(input_file):
    with open( input_file, "r") as fd:
        adapt_list = [int(line.strip())  for line in fd]
    return adapt_list

def count13(adapt):
    c1 = 0
    c3 = 0
    j=0
    for i in adapt:
        if i-j == 1:
            c1+=1
        if i-j==3:
            c3+=1
        j=i    
    return c1, c3

def calc1( input_file):
    adapt_list = crea_list(input_file)
    adapt_list.append( max(adapt_list)+3)
    adapt_list.sort()
    c1, c3 = count13(adapt_list)
    return c1*c3

def dict_previous( adapt_list):
    dicty = {}
    for elt in adapt_list:
        listy=[ elt-(i+1) for i in range(3) if elt-(i+1) in adapt_list]
        dicty[ elt] = listy
    return dicty

# this way is fastest than a recursive function,
# covering each branch only once
def count_combi( dict_prev, adapt_list, maxy):
    dict_c = {}
    dict_c[ maxy] = 1
    for ad in adapt_list:
        dict_c[ ad] = 0
    dict_c[ maxy] = 1
    for ad in adapt_list:
        for prev in dict_prev[ad]:
            dict_c[prev] += dict_c[ad]
    return dict_c[0]
            

def calc2( input_file):
    adapt_list = crea_list(input_file)
    #adapt_list.append( max(adapt_list)+3)
    adapt_list.append( 0)
    adapt_list.sort(reverse=True)
    dict_prev = dict_previous( adapt_list)
    #dict_prev[ max(adapt_list)+3] = [max(adapt_list)]
    count  = count_combi( dict_prev, adapt_list, max(adapt_list))
    return count


print "Test validated: ", test1== calc1( 'test')
print "Test validated: ", test12== calc1( 'test2')
print calc1( 'input')
print "Test validated: ", test2==calc2( 'test')
print "Test validated: ", test22==calc2( 'test2')
print calc2( 'input')

