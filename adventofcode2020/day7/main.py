import numpy as np

#############################
# day 3 

test1=4

#dotted black bags contain no other bags.
# at least one
my_bag='shiny gold'

def rule_interpreter(rule):
    bag = rule.split(" bags ")[0]
    bag_in = rule.split(" contain ")[1].split(', ')
    fro_don = list()
    for elt in bag_in:
        one_bag = elt
        for x in ['no other bags.', '.', ' bags', ' bag']:
            one_bag = one_bag.replace( x, '')
        if one_bag <> '':
            num = int( one_bag.split(' ')[0])
            bagname = one_bag.split( str(num) + ' ')[1]
            fro_don.append( (num,bagname))
    return bag, fro_don   

def list_rule(input_file):
    with open( input_file, "r") as fd:
        board = [line.strip()  for line in fd]
    dic_barule = {}
    for elt in board:
        key, rules = rule_interpreter(elt)
        dic_barule[key] = rules
    return dic_barule

def get_bag_list(dic_barule):
    keys = list(set(dic_barule.keys()))
    for elt in dic_barule.keys():
        for rules in dic_barule[elt]:
            bag = rules[1]
            if not bag in keys:
                    keys.append(bag)
    return keys

def convert2matrix( dic_barule):
    keys = get_bag_list(dic_barule)
    size = len( keys)
    mat = np.zeros((size, size))
    for elt in keys:
        for rules in dic_barule[ elt]:
            mat[ keys.index(elt), keys.index( rules[1])] = rules[0]
    return mat


def calc1(input_file):
    rules = list_rule(input_file)
    keys = get_bag_list(rules)
    my_key = keys.index(my_bag)
    truth = [False for i in range( len(keys))]
    mat = convert2matrix(rules)
    mat0 = mat
    for ii in range(1000):
        truth = [ truth[i] or mat[ i, my_key] >0 for i in range( len(truth))]
        mat = np.dot( mat, mat0)       
    return sum(truth)

    

def calc2(input_file):
    rules = list_rule(input_file)
    keys = get_bag_list(rules)
    my_key = keys.index(my_bag)
    mat = np.transpose( convert2matrix(rules))
    vect = np.zeros( (mat.shape[0]))
    vect[my_key]=1
    count = 0
    for ii in range(100):
        vect = np.dot( mat, vect)
        count = count + vect.sum()
    return count.sum()

print "Test validated: ", test1== calc1( 'test')
print calc1( 'input')
print "Test validated: ", 2==calc2( 'test')
print  "Test validated: ", 126==calc2( 'test2')
print calc2( 'input')

