import numpy as np

#############################
# day 13


test1=295
test2=1068781

def near(bus, early):
    return [ early, ((early // bus)+1)*bus ][ not early % bus ==0]

def calc1(input_file):
    with open( input_file, "r") as fd:
        instru = [line.strip()  for line in fd]
    early = int(instru[0])
    buses = [ int(x) for x in instru[1].split(',') if not x=='x']
    nearest = [ near(bus, early) for bus in buses]
    return  (min( nearest) - early) * buses[ nearest.index( min(nearest))]

def check_old( buses, minbus, maxbus, quotient, printy):
    pos = quotient * maxbus[0]
    checked = True
    for bus in buses:
        checked = checked and ( (pos - maxbus[1] + bus[1]) % bus[0] == 0)
        if printy:
            print "checking ", quotient, bus, (pos - maxbus[1] + bus[1]), (pos - maxbus[1] + bus[1]) % bus[0]
    return checked
    
def check( buses, minbus, maxbus, quotient, list_quotient):
    pos = quotient * maxbus[0]
    for i in range(len( buses)):
        if ( (pos - maxbus[1] + buses[i][1]) % buses[i][0] == 0) and list_quotient[ i]==0:
            list_quotient[ i] = quotient
    return list_quotient


def concat2rules(rules):
    rule1=rules[0]
    rule2=rules[1]
    i1=0
    i2=0
    r1 =0
    r2 =0
    res=0
    r1 = rule1[0] + rule1[1] * i1
    r2 = rule2[0] + rule2[1] * i2
    while r1 <> r2 or res<2:
        if r1 > r2:
            if rule2[0] > rule2[1]:
                i2 = r1 // rule2[1]-1
            else:
                i2 = r1 // rule2[1] 
            if r1 > rule2[0] + rule2[1] * i2:
                i2 +=1
            r2 = rule2[0] + rule2[1] * i2
            #i2+=1
        else:
            r1 = rule1[0] + rule1[1] * i1
            i1+=1
        if r1 == r2 and res==0:
            a = r1 * 1
            res +=1
            r1 = rule1[0] + rule1[1] * i1
            i1+=1
        elif r1 == r2 and res==1:
            b = r1 * 1
            res +=1
    new_rule = (a, b-a)
    list_out = [ new_rule]
    for i in range(len(rules)-2):
        list_out.append( rules[i+2])
    return list_out
        
def calc2(input_file):
    with open( input_file, "r") as fd:
        instru = [line.strip()  for line in fd]
    temp = instru[1].split(',')
    buses = [ ( int(temp[i]), i) for i in range(len(temp)) if not temp[i]=='x']
    val = [ x[0] for x in buses]
    minbus = buses[  0]
    maxbus = buses[  val.index( max(val))]
    maxbus = buses[  -1]
    quotient = 0
    list_quotient = [ 0 for x in buses]
    find_self( buses)
    # find first quotients
    while (0 in list_quotient): #done:
        quotient += 1 #max(list_quotient)
        list_quotient = check( buses, minbus, maxbus, quotient, list_quotient)
    rules = [ (list_quotient[i], buses[i][0]) for i in range(len(list_quotient))]
    for i in range( len(rules)-2):
        rules = concat2rules(rules)
        print rules, rules[0][0] * maxbus[0]- maxbus[1]    
    #check_old( buses, minbus, maxbus, quotient, True)
    return  rules[0][0] * maxbus[0]- maxbus[1]


print "Test validated: ", test1== calc1( 'test')
print calc1( 'input')
print "Test validated: ", 3417==calc2( 'test0')
print "Test validated: ", 754018==calc2( 'test1')
print "Test validated: ", 779210==calc2( 'test2')
print "Test validated: ", 1261476==calc2( 'test3')
print "Test validated: ", 1202161486==calc2( 'test4')
print "Test validated: ", test2==calc2( 'test')
print "\n Calc 2"
print calc2( 'input')

#(17, 0) (409, 17)
#(19, 67) (29, 19)

# 64473997093424308369963 too high
#6315026038759 too low
# 36132160338903420775 too high

