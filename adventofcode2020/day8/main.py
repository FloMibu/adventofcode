import numpy as np

#############################
# day 8 

test1=5
test21=8

def read_instruc(input_file):
    with open( input_file, "r") as fd:
        board = [line.strip()  for line in fd]
    instruct = {}
    for i in range(len(board)):
        instruct[i] = board[i].split(' ') 
    return instruct

def interpret_ins( instruct, accu, index):
    ins = instruct[ index][0]
    if ins == 'acc':
        accu += int(instruct[ index][1] )
        index += 1
    if ins == 'nop':
        index += 1
    if ins == 'jmp':
        index += int(instruct[ index][1] )
    return accu, index

def accu_comp( instruct):
    nb_instruct = len(instruct)
    accu = 0
    index = 0
    instruct_done = [False for i in range(nb_instruct)]
    finish = False
    while not finish:
        instruct_done[ index] = True
        accu_sav = accu * 1
        accu, index = interpret_ins( instruct, accu, index)
        if index <nb_instruct:
            finish = ( instruct_done[ index] ==True)
        if index==nb_instruct:
            break
    return accu, finish


def calc1( input_file):
    instruct = read_instruc(input_file)
    accu, finish = accu_comp( instruct)
    return accu


def fix_by_jmp( instruct):
    fixed = False
    leng = len(instruct)
    for key, value in instruct.items():
        if value[0] == 'nop' and (key + int(value[1])) == leng:
            fixed = True
            instruct[ key][0] = 'jmp'
    return fixed, instruct



def fix_by_nop( instruct):
    leng = len(instruct)
    for key, value in instruct.items():
        fixed = False
        if value[0] == 'jmp' and not fixed:
            ins_test = copy.deepcopy(instruct)    
            ins_test[key][0] = 'nop'
            accu, finish = accu_comp( ins_test)
            if not finish:
                instruct[key][0] = 'nop'
                fixed = True
                break
    return fixed, instruct


def calc2( input_file):
    instruct = read_instruc(input_file)
    fixed, instruct = fix_by_jmp( instruct)
    if not fixed:
        fixed, instruct = fix_by_nop( instruct)
    else:
        print 'fixed with nop'
    if not fixed:
        print 'Nor method fixed the batch'
    else:
        print 'fixed with jmp'
    accu, finish = accu_comp( instruct)
    return accu

print "Test validated: ", test1== calc1( 'test')
print calc1( 'input')
print "Test validated: ", test21==calc2( 'test')
print calc2( 'input')

