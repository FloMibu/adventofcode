import numpy as np

#############################
# day 3 

def nb_trees( input_file, sl1, sl2):
  with open( input_file, "r") as fd:
    mapy = [line.strip()  for line in fd]
  row, col = len(mapy) // sl1, len(mapy[0])
  trees = [ mapy[i*sl1][(i*sl2+1) % (col)-1] == '#' for i in range(row)]
  return sum(trees)

slopy = [1, 3]

print "Test validated: ", 7== nb_trees('test', slopy[0], slopy[1])
print nb_trees('input', slopy[0], slopy[1])

slopy_list = [ (1, 1), (1, 3), (1, 5), (1, 7), (2, 1)] 

print "Test validated: ", 336 == np.prod( [ nb_trees('test',x, y) for (x,y) in slopy_list])
print np.prod( [ nb_trees('input',x, y) for (x,y) in slopy_list])

