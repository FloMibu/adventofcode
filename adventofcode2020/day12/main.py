import numpy as np

#############################
# day 10 


test1=25
test2=286

dict_dir = {'E':0, 'W':180, 'N':90, 'S':270}

# angle, x, y 

def turn_ship( ship, angle):
    ship[0] += angle
    ship[0] = (ship[0]+360) % 360
    return ship

def mv_fwd(ship, val):
    ship[ 1] = ship[ 1] + val * np.cos( float(ship[0]) /180 * np.pi)
    ship[ 2] = ship[ 2] + val * np.sin( float(ship[0]) /180 * np.pi)
    return ship

def mv_dir(ship, val, key):
    angle = dict_dir[ key]
    ship[ 1] = ship[ 1] + val * np.cos( float(angle) /180 * np.pi)
    ship[ 2] = ship[ 2] + val * np.sin( float(angle) /180 * np.pi)
    return ship

def move_ship( ship, instru):
    for elt in instru:
        if 'R' in elt or 'L' in elt:
            ship = turn_ship( ship, int(elt.replace('R','-').replace('L','+') ) )
        elif 'F' in elt:
            ship = mv_fwd(ship, int( elt[1:]))
        else:
            ship = mv_dir(ship, int( elt[1:]), elt[0])
    return ship

def calc1(input_file):
    with open( input_file, "r") as fd:
        instru = [line.strip()  for line in fd]
    ship = [0, 0, 0]
    ship = move_ship( ship, instru)
    ship = [abs(np.around(x,0)) for x in ship]
    ship = [abs(x) for x in ship]
    return  sum(ship[1:])

def rotate_wp( way_point, angle):
    angle = -angle
    way_point[1:] = [ way_point[1] * np.cos( float(angle) /180 * np.pi)  - way_point[2] * np.sin( float(angle) /180 * np.pi),  way_point[1] * np.sin( float(angle) /180 * np.pi)  + way_point[2] * np.cos( float(angle) /180 * np.pi)]
    return way_point
 

def mv_ship2(ship, way_point, val):
    x=way_point[1]
    y=way_point[2]
    ship[ 1] += val* x
    ship[ 2] += val* y
    #way_point[1] -= val* x
    #way_point[2] -= val*y
    return ship, way_point

def calc2(input_file):
    with open( input_file, "r") as fd:
        instru = [line.strip()  for line in fd]
    ship = [0, 0, 0]
    way_point = [0, 10, 1]
    for elt in instru:
        if 'R' in elt or 'L' in elt:
            way_point = rotate_wp( way_point, int(elt.replace('R','+').replace('L','-') ) )
        elif 'F' in elt:
            ship, way_point = mv_ship2(ship, way_point,int( elt[1:]))
        else:
            way_point = mv_dir(way_point, int( elt[1:]), elt[0])
    ship = [abs(x) for x in ship]
    return  sum(ship[1:])


print "Test validated: ", test1== calc1( 'test')
print calc1( 'input')
print "Test validated: ", test2==calc2( 'test')
print calc2( 'input')


#31457 too low
#94904
#33833 too low
