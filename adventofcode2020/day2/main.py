import numpy as np
import copy
import matplotlib.pyplot as mplt
from scipy.optimize import fsolve
import itertools as it

#############################
# day 1 

def parse_pwd(stry):
    miny = stry.split(' ')[0].split('-')[0]
    maxy = stry.split(' ')[0].split('-')[1]
    key = stry.split(' ')[1][0]
    pwd = stry.split(' ')[2]
    return int(miny), int(maxy), key, pwd

def check_pwd( stry, rule): 
   miny, maxy, key, pwd = parse_pwd(stry)
   count = pwd.count(key)
   if rule ==1:
       booly = (count >= miny and count <= maxy)
   if rule ==2:
       a=pwd[miny-1] == key
       b=pwd[maxy-1] == key
       booly = (a and not b) or (not a and b)
   if booly:
       return False #pwd ok
   else:
       return True # found wrong pwd

def check_list( input_file, rule):
   listy = np.genfromtxt( input_file, dtype='str', delimiter='|').tolist()
   return [check_pwd(x, rule) for x in listy]

print "Test validated: ", 2==sum( [not x for x in check_list('test', 1)])
print sum( [not x for x in check_list('input', 1)])

print "Test validated: ", 2==sum( [not x for x in check_list('test', 2)])
print sum( [not x for x in check_list('input', 2)])


