import numpy as np

#############################
# day 6

test0=6
test=11

def split_in_group(input_f):
    stry = open(input_f).read()
    list_group = [x.replace('\n','') for x in stry.split('\n\n')]
    return list_group

def count(elt):
    return len(set(elt))
    
def calc1(input_f):
    return sum( [ count(x) for x in split_in_group(input_f)])

# need to split groups by person now.. I knew it.. ****
def calc2(input_f):
    stry = open(input_f).read()
    stry=stry[0:-1]
    list_group = [x for x in stry.split('\n\n')]
    total = 0
    for elt in list_group:
        nb_person = elt.count('\n') + 1
        elt0 = elt.replace('\n','')
        for x in set(elt0):
            if elt0.count(x) == nb_person:
                total+=1
    return total


print "Test validated: ", test0 == calc1('test0')
print "Test validated: ", test == calc1('test')

print calc1('input')

print "Test validated: ", 6 == calc2('test')
print calc2('input')
