import numpy as np

#############################
# day 10 


test1=37
test2=26

def crea_mat(input_file):
    with open( input_file, "r") as fd:
        info = [line.strip() for line in fd]
    lin, col = len(info), len( info[0])
    mat = np.zeros((lin, col))
    mask = np.zeros((lin, col))
    for i in range(lin):
        for j in range(col):
            if info[i][j] == 'L':
                mat[ i, j] = 0
            if info[i][j] == '.':
                mask[ i, j] = 1
    mask = np.where( mask==1)
    return mask, mat

def gameoflife( mat):
    mat0 = 1*mat
    lin, col = mat.shape[0], mat.shape[1]
    mat[1:,:] = mat[1:,:] + mat0[0:-1,:]
    mat[0:-1,:] = mat[0:-1,:] + mat0[1:,:]
    mat[:,1:] = mat[:,1:] + mat0[:,0:-1]
    mat[:,0:-1] = mat[:,0:-1] + mat0[:,1:]
    mat[0:-1, 1:] = mat[0:-1, 1:] + mat0[1:,0:-1]
    mat[0:-1, 0:-1] = mat[0:-1, 0:-1] + mat0[1:,1:]
    mat[1:, 1:] = mat[1:, 1:] + mat0[0:-1,0:-1]
    mat[1:, 0:-1] = mat[1:, 0:-1] + mat0[0:-1,1:]
    ind_temp = np.where( mat==0)
    mat[ np.where( mat>0) and np.where( mat0==0)] = 0
    mat[ ind_temp] = 1
    mat[ np.where( mat>=5)] = 0
    mat[ np.where( mat>=1)] = 1
    return mat

def reset_floor( mat, mask):
    mat[ mask] = 0
    return mat

def calc1( input_file):
    mask, mat = crea_mat(input_file)
    check = np.sum(mat) + 1
    while check<> np.sum(mat):
        check = np.sum(mat)
        mat = gameoflife( mat)
        mat = reset_floor( mat, mask)
    return np.sum(mat)

print "Test validated: ", test1== calc1( 'test')
print calc1( 'input')
#print "Test validated: ", test2==calc2( 'test')
#print calc2( 'input')

