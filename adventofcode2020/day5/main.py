import numpy as np

#############################
# day 3 

test1=['BFFFBBFRRR', 70, 7, 567.]
test2=['FFFBBBFRRR',14,  7, 119.]
test3=['BBFFBBFRLL', 102,  4, 820]

def convertrow(rowin, key):
    out=0
    for ii in range(len(rowin)):
        out = out + (rowin[ii] == key ) * 2** (len(rowin)-ii-1)
    return out

def bin_space(stry):
    row = convertrow( stry[0:7], 'B')
    col =  convertrow( stry[7:], 'R')
    return row*8+col

def boarding(input_file):
    with open( input_file, "r") as fd:
        board = [line.strip()  for line in fd]
    seats = [ bin_space(x) for x in board]
    return seats

def calc1(input_file):
    return max( boarding(input_file))

def calc2(input_file):
    seats = boarding(input_file)
    maxy = max( seats)
    for x in range(8)+[(maxy // 8) * 8 +i for i in range(8)]:
        if x in seats:
            seats.remove(x)
    num = min(seats)
    for i in range(len(seats)):
            num = num+1
            if not num in seats:
                print num

print "Test validated: ", test1[3]== bin_space(test1[0])
print "Test validated: ", test2[3]== bin_space(test2[0])
print "Test validated: ", test3[3]== bin_space(test3[0])

print calc1('input')
print calc2('input')
