import numpy as np

#############################
# day 3 

fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid', 'cid']
fields.remove('cid')    
test0=2;
test1=0;

def parse_file(fily):
    passp_list = list()
    new = True
    for elt in fily:
        if elt == '':
            new=True
        elif new==True:
            passp_list.append( elt)
            new=False
        else:
            passp_list[-1] = passp_list[-1] + ' ' + elt
    return passp_list

def parse_pass(elt):
    dicy = {}
    for x in elt.split(' '):
        dicy[ x.split(':')[0]] = x.split(':')[1]
    return dicy

def open_pass( input_file):
    with open( input_file, "r") as fd:
        fily = [line.strip()  for line in fd]
    passp_list = parse_file(fily)
    passp = [ parse_pass(x) for x in passp_list]
    return passp

def present_fields( passp):
    #present_pass = [ True for x in passp if sum([y in x.keys() for y in fields])==len(fields)]
    present_pass = [ sum([y in x.keys() for y in fields])==len(fields) for x in passp]
    return present_pass

def calc1( input_file):
    passp = open_pass( input_file)
    return sum( present_fields( passp))

rule1 = {'byr': [1920, 2002, 4], 'iyr':[2010, 2020, 4], 'eyr': [ 2020, 2030, 4], 'pid': [0, 999999999, 9]}
rule_hgt = {'cm': [150, 193], 'in':[59, 76]}
rule_eye = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']

def check_range( dic, key, elt):
    return dic[key][0] <= elt <= dic[key][1]

def check_field(x):
    valid = True
    for fld in rule1.keys(): # include pid
        if fld in x.keys():
            try:
                valid = valid and check_range( rule1, fld, int(x[fld])) and len(x[fld]) == rule1[fld][2]
            except:
                valid = valid and False
    if 'hgt' in x.keys():
        for elt in ['cm', 'in']:
            if elt in x['hgt']:
                valid = valid and check_range( rule_hgt, elt, int( x['hgt'].split( elt)[0]))
        valid = valid and sum( [elt in x['hgt'] for elt in ['cm', 'in']]) >=1
    #hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    if 'hcl' in x.keys():
        valid = valid and ( len( x['hcl'].split('#')[0]) <=6 and x['hcl'][0] == '#') #and not ('z' in x['hcl'])  # regexp bitch
    if 'ecl' in x.keys():
        valid = valid and ( x['ecl'] in rule_eye)
    return valid

def valid_fields( passp):
    fields_valid = [ check_field(x) for x in passp]
    return fields_valid

def calcl2( input_file):
    passp = open_pass( input_file)
    pr = present_fields( passp)
    vd = valid_fields( passp)
    print 
    psNvd = [ pr[i] and vd[i] for i in range(len(passp))]
    return sum( psNvd)
   

print "Test validated: ", test0 == calc1( 'test')
print calc1( 'input')

print "Test validated: ", 0== calcl2( 'test2')
print "Test validated: ", 4== calcl2( 'test3')
print calcl2( 'input')


