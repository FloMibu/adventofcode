import numpy as np
import copy
import matplotlib.pyplot as mplt
from scipy.optimize import fsolve

#############################
# day 1 - 1

def find_entries( input_file, match):
   er = np.genfromtxt( input_file, dtype='int', delimiter='|').tolist()
   solutions = [er[i]*j for i in range(len(er)-1) for j in er[(i+1):] if er[i]+j==match]  
   return solutions

print find_entries('test', 2020)[0] == 514579
print find_entries('input', 2020)

def find_entries( input_file, match):
   er = np.genfromtxt( input_file, dtype='int', delimiter='|').tolist()
   #solutions = [er[i]*j for i in range(len(er)-1) for j in er[(i+1):] if er[i]+j==match]
   solutions = [er[i]*er[i+j]*k for i in range(len(er)-1) for j in range(len(er[(i+1):])-1) for k in er[(i+j+1):] if er[i]+er[i+j]+k==match]
   return solutions

print find_entries('test', 2020)[0] == 241861950
print find_entries('input', 2020)

# TODO
# code one fonction with a third argument : 2 or 3
