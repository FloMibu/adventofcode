import numpy as np

#############################
# day 8 

test1=127
test21=62

def list_num(input_file):
    with open( input_file, "r") as fd:
        num = [int(line.strip())  for line in fd]
    return num

def preamble( num, preamble_num, ind):
    add = ind-preamble_num
    return [ num[i+add] + num[j+add] for i in range(preamble_num) for j in range(preamble_num) if num[i+add]<>num[j+add] ]

def calc1( input_file, rule):
    num = list_num(input_file)
    i=rule
    while num[i] in preamble(num, rule,i):
        i+=1
    return num[i]

def tryit( num, i, listy, obj):
    listy = listy[1:] # remove first elt
    j = i + len(listy)
    go_fwd = sum(listy) < obj
    if go_fwd:
        while sum(listy) < obj:
            j+=1
            listy.append( num[j])
    else:        
        while sum(listy) > obj:
            listy = listy[0:-1]
    found= sum(listy) == obj
    return listy, found      

def calc2( input_file, rule):
    res = calc1( input_file, rule)
    num = list_num(input_file)
    i=0
    found = False
    listy = [0,num[i]]
    while not found:
        listy, found = tryit( num, i, listy, res)
        i+=1
    return max(listy) + min(listy)

print "Test validated: ", test1== calc1( 'test', 5)
print calc1( 'input', 25)
print "Test validated: ", test21==calc2( 'test', 5)
print calc2( 'input', 25)

