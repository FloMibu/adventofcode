
#############################
# day 13


test1=165
test2=208

def newmask( ins):
    mask = {}
    bitmask = ins.split('mask = ')[1]
    for i in range(len(bitmask)-1, -1, -1):
        if bitmask[i] <> 'X':
            mask[36-1-i] = int(bitmask[i])
    return mask

def change_mem( mem, mask, ins):
    adress = int(ins.split('] = ')[0].replace('mem[',''))
    value = int(ins.split('] = ')[1])
    value2 = 1*value
    i = 0
    maskkeys = mask.keys()
    for i in range( 36-1, -1, -1):
        power = value2 // 2**i
        value2 -= power* 2**i
        if i in maskkeys:
            if mask[i] <> power:
                value += (mask[i]-power) * 2**i
    mem[adress] = value
    return mem

def write_adresses( mem, bit_adress, value, maskkeys, i0):
    done = True
    for i in range( i0-1, -1, -1):
            if i not in maskkeys:
                done = False
                new_mask = [x for x in maskkeys ]
                new_mask.append(i)
                bit_adress[i] = 0
                mem = write_adresses( mem, bit_adress, value, new_mask, i)
                bit_adress[i] = 1
                mem = write_adresses( mem, bit_adress, value, new_mask, i)
                break
    if done:#i0==-1 or not go_rec:
        index = 0
        for i in range( 36-1, -1, -1):
            index += bit_adress[i] * 2 **i
        mem[index] = value
    return mem


def change_mem2( mem, mask, ins):
    adress = int(ins.split('] = ')[0].replace('mem[',''))
    value = int(ins.split('] = ')[1])
    value2 = 1*adress
    i = 0
    # adress to bit
    bit_adress={}
    maskkeys = mask.keys()
    for i in range( 36-1, -1, -1):
        power = value2 // 2**i
        value2 -= power* 2**i
        if i in maskkeys:
            bit_adress[i] =  [ power , 1][ mask[i]]
    mem = write_adresses( mem, bit_adress, value, mask.keys(), 36)
    return mem

def calc(input_file, change_mem):
    with open( input_file, "r") as fd:
        instru = [line.strip()  for line in fd]
    mem = {}
    for ins in instru:
        if 'mask' in ins:
            mask = newmask( ins)
        else:
            mem = change_mem( mem, mask, ins)
    sumy = 0
    for elt in mem.items():
        sumy += elt[1]
    return sumy



print "Test validated: ", test1== calc( 'test', change_mem)
print calc( 'input', change_mem)
print "Test validated: ", test2==calc( 'test2', change_mem2)
print calc( 'input', change_mem2)





